
public class FuncionesRecursivas
{
      int fibonaci(int n){
       if (n < 3){ 
         return 1;
       }
      return fibonaci(n-1) + fibonaci(n-2);
   
    }
    
    
    int factorial(int n){
        if (n <2){
           return 1;
        }
        return n * factorial(n-1);
    }
    
}
