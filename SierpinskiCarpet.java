import java.awt.*;import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.Scanner;


public class SierpinskiCarpet extends JPanel {
    private final int dim = 513;
    private final int margin = 20;
    private static Color colorRelleno;
    private static Color colorFondo;
    private int limit = dim;


    public SierpinskiCarpet() {
        setPreferredSize(new Dimension(dim + 2 * margin, dim + 2 * margin));
        setBackground(colorFondo);
        setForeground(colorRelleno);
        new Timer(2000, (ActionEvent e) -> {
                limit /= 3;
                if (limit <= 3)
                    limit = dim;
                repaint();
            }).start();
    }


    void drawCarpet(Graphics2D g, int x, int y, int size) {
        if (size < limit)
            return;
        size /= 3;
        for (int i = 0; i < 9; i++) {
            if (i == 4) {
                g.fillRect(x + size, y + size, size, size);
            } else {
                drawCarpet(g, x + (i % 3) * size, y + (i / 3) * size, size);
            }
        }
    }


    @Override
    public void paintComponent(Graphics gg) {
        super.paintComponent(gg);
        Graphics2D g = (Graphics2D) gg;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g.translate(margin, margin);
        drawCarpet(g, 0, 0, dim);
    }


    public static void main(String[] args) {
        Color color1 = Color.blue; Color color2 = Color.orange;Color color3 = Color.red;Color color4 = Color.black;
        Color color5 = Color.yellow;Color color6 = Color.green;Color color7 = Color.pink;
       
        System.out.println("Elije el color del relleno de la figura: ");
        Scanner sc = new Scanner(System.in);
       
        System.out.println("Elije el color del fondo de la figura: ");
        Scanner fo = new Scanner(System.in);
       
         String elegido2 = sc.nextLine();
        if (elegido2.equals("azul")){
            colorRelleno = color1;
        }
       if (elegido2.equals("rojo")){
            colorRelleno = color3;
       }
       if (elegido2.equals("naranja")){
            colorRelleno = color2;
       }
       if (elegido2.equals("negro")){
            colorRelleno = color4;
       }
       if (elegido2.equals("amarillo")){
           colorRelleno = color5;
       }
       if (elegido2.equals("verde")){
          colorRelleno = color6;
        }
       if (elegido2.equals("rosa")){
           colorRelleno = color7;
        }
       
        String elegido = fo.nextLine();
        if (elegido.equals("azul")){
            colorFondo = color1;
        }
       if (elegido.equals("rojo")){
            colorFondo = color3;
       }
       if (elegido.equals("naranja")){
            colorFondo = color2;
       }
       if (elegido.equals("negro")){
            colorFondo = color4;
       }
       if (elegido.equals("amarillo")){
            colorFondo = color5;
       }
       if (elegido.equals("verde")){
            colorFondo = color6;
        }
       if (elegido.equals("rosa")){
            colorFondo = color7;
        }


       
        SwingUtilities.invokeLater(() -> {
                JFrame f = new JFrame();
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setTitle("Sierpinski Carpet");
                f.setResizable(false);
                f.add(new SierpinskiCarpet(), BorderLayout.CENTER);
                f.pack();
                f.setLocationRelativeTo(null);
                f.setVisible(true);
            });
    }
}
