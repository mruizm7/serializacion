import java.util.*;
import java.io.*;
public class ControladorAgenda
{
   public boolean guardarAgenda(Object a){
        ArrayList b = (ArrayList)a;
        boolean ban = false;
        FileOutputStream fos = null;
        ObjectOutputStream salida = null;
        
        try{
           fos = new FileOutputStream("agenda.ser");
           salida = new ObjectOutputStream(fos);
           
           salida.writeObject(b);
           ban =  true;
        }
        
        catch(Exception e){
            e.printStackTrace();
            ban =  false;
        }
        
        finally{
            try{
               if (salida != null)
                  salida.close();
               if (fos != null)
                  fos.close();
                  return ban;
            }
            catch(IOException e){
               e.printStackTrace();
               return ban;
            }
        }
        
    }
    
    public ArrayList obtenObjetoDeArchivo(){
        ArrayList tmp = null;
        FileInputStream fis = null;
        ObjectInputStream entrada = null;
        
        try{
           fis = new FileInputStream("agenda.ser");
           entrada = new ObjectInputStream(fis);
           
           tmp =  (ArrayList)entrada.readObject();
        }
                catch(Exception e ){
            e.printStackTrace();
        }
        
        finally{
            try{
               if (entrada != null)
                  entrada.close();
               if (fis != null)
                  fis.close();
            }
            catch(IOException e){
               e.printStackTrace();
            }
        }
        
        return tmp;
    } 
    
    
}
